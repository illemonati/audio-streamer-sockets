import pyaudio
import wave
import socket
import argparse

parser = argparse.ArgumentParser(description='Audio Streamer')

parser.add_argument("-t", "--type", help="type (client|server)")
parser.add_argument("-r", "--host", help="host")
parser.add_argument("-p", "--port", help="port", type=int)

audio = pyaudio.PyAudio()

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 2
RATE = 44100


PORT = 33317


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


def server(host=None, port=None):

    s.bind((host or "0.0.0.0", port or PORT))

    s.listen(1)
    conn, client_addr = s.accept()

    stream = audio.open(format=FORMAT, channels=CHANNELS,
                        rate=RATE, input=True, frames_per_buffer=CHUNK)

    print(client_addr)

    while True:
        data = stream.read(CHUNK)
        conn.send(data)


def client(host=None, port=None):
    stream = audio.open(format=FORMAT,
                        channels=CHANNELS,
                        rate=RATE,
                        frames_per_buffer=CHUNK,
                        output=True)

    s.connect((host or "0.0.0.0", port or PORT))

    while True:
        data = s.recv(CHUNK)
        stream.write(data)

    s.close()


if __name__ == '__main__':
    args = vars(parser.parse_args())
    print(args['host'])
    if args['type'] == 'server':
        server(args['host'], args['port'])
    elif args['type'] == 'client':
        client(args['host'], args['port'])
    print('wrong type')
